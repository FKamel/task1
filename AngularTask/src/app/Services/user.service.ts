import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfirmEmail } from '../Shared Calasses/ConfirmEmail';
import { LoginUser } from '../Shared Calasses/Login';
import { ResetPassword } from '../Shared Calasses/ResetPassword';
import { User } from '../Shared Calasses/User';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  url_Register='https://localhost:44356/api/Auth/Register?isAdmin=true';
  url_Login='https://localhost:44356/api/Auth/Login';
  url_confirm="https://localhost:44356/api/Auth/EmailConfrimation";
  url_reset='https://localhost:44356/api/Auth/ResetPassword';
  ngOnInit() {          
  }
  registerUser(user : User)
  {
    return this.http.post(this.url_Register,user);
  }
  loginUser(loginuser : LoginUser)
  {
    return this.http.post(this.url_Login,loginuser);
  }
  confirmEmail(confirmemail : ConfirmEmail)
  {
    return this.http.post(this. url_confirm,confirmemail);
  }
  resetpassword(resetPassword : ResetPassword)
  {
    return this.http.put(this.url_reset,resetPassword);
  }
}
  