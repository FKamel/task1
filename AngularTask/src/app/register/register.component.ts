import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../Services/user.service';
import { User } from '../Shared Calasses/User';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  userModel=new User('','','','');
  constructor(private userservice:UserService,private router: Router) { }

  ngOnInit(): void {
    this.reserform();
  }
  reserform(form? : NgForm){
    if(form !=null)
      form.reset();
    this.userModel= {
      userName:'',
      email:'',
      password : '',
      confirmPassword :''
    }
  }
  public HasError: boolean;
  public errorMsg: string;
  OnSubmit(form : NgForm){
    console.log(this.userModel);
    this.userservice.registerUser(this.userModel).subscribe((data:any)=>{
      if(data.Succeeded == true)
      this.reserform(form);
      this.router.navigate(["/EmailConfirmation"]);
    },error => {
      this.HasError = true;
      this.errorMsg = error;
    })
  }

}
