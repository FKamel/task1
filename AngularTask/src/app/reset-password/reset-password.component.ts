import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../Services/user.service';
import { ResetPassword } from '../Shared Calasses/ResetPassword';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  resetModel=new ResetPassword('','','');
  constructor(private userservice:UserService,private router: Router) { }
  
  ngOnInit(): void {
    this.reserform();
  }
  reserform(form? : NgForm){
    if(form !=null)
      form.reset();
    this.resetModel= {
      email:'',
      password:'',
      confirmPassword:''
    }
  }
  public showError: boolean;
  public errorMessage: string;
  OnSubmit(form : NgForm){
    console.log(this.resetModel);
    this.userservice.resetpassword(this.resetModel).subscribe((data:any)=>{
      if(data.error == null)
        this.reserform(form);
        this.router.navigate(["/Login"]);
    },error => {
      this.showError = true;
      this.errorMessage = error;
    })
  }

}

