import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../Services/user.service';
import { LoginUser } from '../Shared Calasses/Login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginuser=new LoginUser('','');
  constructor(private userservice:UserService,private router: Router) { }

  ngOnInit(): void {
    this.reserform();
  }
  reserform(form? : NgForm){
    if(form !=null)
      form.reset();
    this.loginuser= {
      email:'',
      password : ''
    }
  }
  public HasError: boolean;
  public errorMsg: string;
  OnSubmit(form : NgForm){
  this.userservice.loginUser(this.loginuser).subscribe((data:any)=>{
    if(data.Succeeded == true)
      this.reserform(form);
      this.router.navigate(["/Register"]);
  },error => {
    this.HasError = true;
    this.errorMsg = error;
  })
  
  }
}
