import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../Services/user.service';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.css']
})
export class ConfirmEmailComponent implements OnInit {

  constructor(private userservice:UserService,private router: Router) { }

  ngOnInit(): void {
  }
  
  public success: boolean;
  public HasError: boolean;
  public errorMsg: string;

  OnSubmit(form : NgForm){
    
    console.log(form.value);
    this.userservice.confirmEmail(form.value).subscribe((data:any)=>{
      if(data.error == null)
        this.success=true;
    },error => {
      this.HasError = true;
      this.errorMsg = error;
      this.success =false;
    })
}
}
