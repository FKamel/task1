﻿using Task1_API.Dto;
using Task1_API.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Task1_API.Configuration;
using Microsoft.AspNetCore.Identity;
using Task1_API.Models;

namespace Task1_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IAuthService _authService;
        private readonly IEmailService _emailService;
        public AuthController(IAuthService authService, IEmailService emailService, UserManager<AppUser> userManager)
        {
            _authService = authService;
            _emailService = emailService;
            _userManager = userManager;
        }

        [HttpPost("Register")]
        public async Task<IActionResult> RegisterAsync([FromBody] RegisterModel model, bool isAdmin = false)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _authService.RegisterAsync(model, isAdmin);

            if (!result.IsAuthenticated)
                return BadRequest(result.Message);

            return Ok(result);
        }

        [HttpPost("Login")]
        public async Task<IActionResult> GetTokenAsync([FromBody] TokenRequestModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _authService.GetTokenAsync(model);

            if (!result.IsAuthenticated)
                return BadRequest(result.Message);

            return Ok(result);
        }
        [HttpPost("EmailConfrimation")]
        public async Task<IActionResult> EmailConfrimationAsync(ConfirmEmailModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _userManager.FindByEmailAsync(model.Email);
            if (result is null)
                return BadRequest("Invalid Email OR Not Register");
            else
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                UserData userData = new UserData();
                userData.Email = model.Email;
                userData.UserName = user.UserName;
                var resultEmail = _emailService.SendMessageToConfirmEmail(userData);
                if (resultEmail == true)
                {
                    await _userManager.SetTwoFactorEnabledAsync(user, true);
                    user.EmailConfirmed = true;
                    await _userManager.UpdateAsync(user);
                }
            }
            return Ok(result);
        }
        [HttpPost("SendMessageToEmail")]
        public bool SendMessageToEmail([FromForm] UserData userData)
        {
            return _emailService.SendMessageToConfirmEmail(userData);
        }
        [HttpPut("ResetPassword")]
        public async Task<IActionResult> ResetPassword( ResetPasswordModel model)
        {
           /// return BadRequest("noooooooooooooo");

            //if (!ModelState.IsValid)
            //  return BadRequest(ModelState);

            //if (model.Password != model.ConfrimPassword)
            //    return BadRequest("Password and Confirm Password Not Match");
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user is null)
                return BadRequest("Invaild Email");
            var hashedNewPassword = _userManager.PasswordHasher.HashPassword(user,model.Password);
            user.PasswordHash = hashedNewPassword;
            await _userManager.UpdateAsync(user);
            return Ok(user);
        }
    }
}