﻿
namespace Task1_API.Dto
{
    public class TokenRequestModel
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}