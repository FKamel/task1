﻿using System.ComponentModel.DataAnnotations;

namespace Task1_API.Dto
{
    public class RoleModel
    {
        public string Email { get; set; }
        public string Role { get; set; }
    }
}