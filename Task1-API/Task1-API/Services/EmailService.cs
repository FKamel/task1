﻿using Task1_API.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;

namespace Task1_API.Services
{ 
    public class EmailService : IEmailService
    {
        EmailSettings _emailSettings = null;
        public EmailService(IOptions<EmailSettings> options)
        {
            _emailSettings = options.Value;
        }
        public bool SendMessageToConfirmEmail(UserData userData)
        {
            try
            {
                var fromAddress = new MailAddress("mariam.mustaf2184@gmail.com", "Kanf Website");
                var toAddress = new MailAddress(userData.Email, userData.UserName);
                const string fromPassword = "Coutmariam11$";
                const string subject = "Kanf Website";
                const string body = "Welcome in our Website Click here http://localhost:2146/Login to Confrim Your Email";
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
                return true;    
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
