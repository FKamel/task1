﻿using System.Threading.Tasks;
using Task1_API.Dto;
using Task1_API.Models;

namespace Task1_API.Services
{
    public interface IAuthService
    {
        Task<AuthModel> RegisterAsync(RegisterModel model, bool isAdmin);
        Task<AuthModel> GetTokenAsync(TokenRequestModel model);
        Task<AuthModel> ResetPasswordAsync(ResetPasswordModel model);
    }
}