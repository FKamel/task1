﻿using System.Threading.Tasks;
using Task1_API.Configuration;

namespace Task1_API.Services
{
    public interface IEmailService
    {
        public bool SendMessageToConfirmEmail(UserData userData);
    }
}
